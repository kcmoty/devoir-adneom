package com.adneom.devoir;

import java.util.AbstractList;
import java.util.List;

/**
 * @author kcmoty
 */

public class Partition {

	/**
	 * <b>Methode de Partition</b>
	 * 
	 * @param liste
	 *            La liste � partitionner
	 * @param taille
	 *            La taille maximum d'une partition
	 * 
	 * @return Une liste des sous-listes(partitions)
	 * 
	 * @throws NullPointerException
	 *             Pour controler les listes null
	 * 
	 * @throws IllegalArgumentException
	 *             Pour contr�ler le contenu de la liste et la valeur du param
	 *             taille
	 */
	public static <T> List<List<T>> partition(List<T> liste, int taille) {
		if (liste == null)
			throw new NullPointerException(" La liste ne doit pas etre null");

		if (!(liste.size() > 0))
			throw new IllegalArgumentException("La liste doit contenir au moins un objet");

		if (!(taille > 0))
			throw new IllegalArgumentException("La taille doit etre superieure � 0");

		if (taille > liste.size())
			throw new IllegalArgumentException("La 'taille' doit etre inferieure ou egale � " + liste.size());

		return new Partitions<T>(liste, taille);
	}

	private static class Partitions<T> extends AbstractList<List<T>> {

		final List<T> liste;
		final int taille;

		Partitions(List<T> liste, int taille) {
			this.liste = liste;
			this.taille = taille;
		}

		/**
		 * Impl�mentation de la methode Get de AbstractList
		 * 
		 * @param index
		 *            Index de l'�l�ment � retourner
		 * @return Les sous-listes de la liste initiale
		 */
		@Override
		public List<T> get(int index) {

			if (index < 0 || index >= size())
				throw new IndexOutOfBoundsException(" L'index " + index + "doit etre compris entre 0 et " + size());

			int debut = index * taille;
			int fin = Math.min(debut + taille, liste.size());
			return liste.subList(debut, fin);
		}

		/**
		 * Impl�mentation de la methode Size de AbstractList
		 *
		 * @return Nombre de partitions � retourner
		 */
		@Override
		public int size() {
			return (liste.size() + taille - 1) / taille;
		}

	}

}
