package com.adneom.devoir;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class PartitionTest {
	List<String> liste = new ArrayList<String>();

	/**
	 * Test du NullPointerException sur le param liste si null
	 */
	@Test(expected = NullPointerException.class)
	public void TesterListeNull() {
		liste = null;
		List<List<String>> partition = Partition.partition(liste, 2);
		System.out.println(partition);
	}

	/**
	 * Test du IllegalArgumentException sur le param liste si vide
	 */
	@Test(expected = IllegalArgumentException.class)
	public void TesterListeVide() {
		liste = new ArrayList<String>();
		List<List<String>> partition = Partition.partition(liste, 2);
		System.out.println(partition);
	}

	/**
	 * Test du IllegalArgumentException sur la valeur du param taille si elle
	 * est pas superieure � 0
	 */
	@Test(expected = IllegalArgumentException.class)
	public void TesterTaillePositive() {
		liste = new ArrayList<String>();
		List<List<String>> partition = Partition.partition(liste, -2);
		System.out.println(partition);
	}

	/**
	 * Test du IllegalArgumentException sur la valeur du param taille si elle
	 * est superieure � la longueur du param liste
	 */
	@Test(expected = IllegalArgumentException.class)
	public void TesterTaillemaximum() {
		liste = new ArrayList<String>();
		liste.add("1");
		liste.add("2");
		liste.add("3");
		liste.add("4");
		List<List<String>> partition = Partition.partition(liste, 5);
		System.out.println(partition);
	}

	/**
	 * Test de la m�thode partition
	 */
	@Test
	public void TesterPartition() {
		liste = new ArrayList<String>();
		liste.add("1");
		liste.add("2");
		liste.add("3");
		liste.add("4");
		liste.add("5");

		List<List<String>> partition = Partition.partition(liste, 2);
		System.out.println("Partition (" + liste + ",2) retourne : " + partition);
		assertTrue(partition.size() == 3);

		partition = Partition.partition(liste, 3);
		System.out.println("Partition (" + liste + ",3) retourne : " + partition);
		assertTrue(partition.size() == 2);

		partition = Partition.partition(liste, 1);
		System.out.println("Partition (" + liste + ",1) retourne : " + partition);
		assertTrue(partition.size() == 5);
	}

}
